﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Connection;
using WebApplicationHerdez.Models;
using WebAppHerdez.DTOs.Productos;

namespace WebApplicationHerdez.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IWebHostEnvironment _host;
        private readonly IHttpContextAccessor _httpContext;
        private readonly string Api = "https://localhost:44359/api/Producto";
        public HomeController(ILogger<HomeController> logger, IWebHostEnvironment host, IHttpContextAccessor httpContext)
        {
            _logger = logger;
            _host = host;
            _httpContext = httpContext;
        }

        public IActionResult Index()
        {
            HttpRequest Request = _httpContext.HttpContext.Request;
            string GetUriServer = $"{Request.Scheme}://{Request.Host}{Request.PathBase.Value.TrimEnd('/')}/";
            ViewBag.Host = String.IsNullOrEmpty(GetUriServer) ? String.Empty : GetUriServer;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public ActionResult GetProducto(ProductoDTO xdata)
        {
            try
            {
                var Output = ConnectionService.Post(xdata, $"{Api}/GetProducto");
                ProductoDTO producto = JsonConvert.DeserializeObject<ProductoDTO>(Output.Body);
                Output.Body = "Correcto!";
                Output.ObjectGeneric = producto;
                return Json(new { data = Output });

            }
            catch (Exception ex)
            {
                var error = ex;
                ResponseDTO response = new ResponseDTO { IdResponse = 0, Body = "La peración no se puede realizar en este momento" };
                return Json(new { data = response });
            }
        }

        [HttpGet]
        public ActionResult GetProductosLeft()
        {
            try
            {
                var Output = ConnectionService.Get(null, $"{Api}/GetProductosLeft");
                List<ProductoDTO> list = JsonConvert.DeserializeObject<List<ProductoDTO>>(Output.Body);
                Output.Body = "Correcto!";
                Output.ObjectGeneric = list;
                return Json(new { data = Output });

            }
            catch (Exception)
            {
                ResponseDTO response = new ResponseDTO { IdResponse = 0, Body = "La peración no se puede realizar en este momento" };
                return Json(new { data = response });
            }
        }

        [HttpGet]
        public ActionResult GetProductosRight()
        {
            try
            {
                var Output = ConnectionService.Get(null, $"{Api}/GetProductosRight");
                List<ProductoDTO> list = JsonConvert.DeserializeObject<List<ProductoDTO>>(Output.Body);
                Output.Body = "Correcto!";
                Output.ObjectGeneric = list;
                return Json(new { data = Output });

            }
            catch (Exception)
            {
                ResponseDTO response = new ResponseDTO { IdResponse = 0, Body = "La peración no se puede realizar en este momento" };
                return Json(new { data = response });
            }
        }
    }
}
