﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WebAppHerdez.DTOs;
using WebAppHerdez.DTOs.Productos;

namespace WebApplication.Connection
{
    public static class ConnectionService
    {
        public static ResponseDTO Post(object input, String URL)
        {
            ResponseDTO response = new ResponseDTO();

            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                if (input != null)
                {
                    using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        streamWriter.Write(JsonConvert.SerializeObject(input));
                    }
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                if (httpResponse != null)
                {
                    using (StreamReader streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        response.Body = streamReader.ReadToEnd();
                    }
                    if (httpResponse.StatusCode == HttpStatusCode.OK)
                    {
                        response.IdResponse = 1;
                    }
                    return response;
                }
                else
                {
                    response.Body = "La peración no se puede realizar en este momento";
                    return response;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        public static ResponseDTO Get(object input, String URL)
        {
            ResponseDTO response = new ResponseDTO();

            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";

                if (input != null)
                {
                    using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        streamWriter.Write(JsonConvert.SerializeObject(input));
                    }
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                if (httpResponse != null)
                {
                    using (StreamReader streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        response.Body = streamReader.ReadToEnd();
                    }
                    if (httpResponse.StatusCode == HttpStatusCode.OK)
                    {
                        response.IdResponse = 1;
                    }
                    return response;
                }
                else
                {
                    response.Body = "La peración no se puede realizar en este momento";
                    return response;
                }

            }
            catch (Exception)
            {
                 throw;
            }
        }
    }
}
