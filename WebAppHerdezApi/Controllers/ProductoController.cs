﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using WebAppHerdez.DTOs;
using WebAppHerdez.DTOs.Productos;
using WebAppHerdez.Services.Productos;

namespace WebAppHerdezApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : ControllerBase
    {
        private readonly IProductoDataService dataService;

        public ProductoController(IProductoDataService _dataServices)
        {
            dataService = _dataServices;
        }

        [HttpPost("GetProducto")]
        public IActionResult GetProducto([FromBody] ProductoDTO input)
        {
            try
            {
                Response _result = null;
                Response _result2 = null;
                string url = $"https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF46410/datos/2021-12-22/2021-12-22";
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.Accept = "application/json";
                request.Headers["Bmx-Token"] = "32e42f119b1b41c6eed92bf39c4cc5e5bd6706778c2c036ceb087cceb01bc5a0";
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception(String.Format(
                    "Server error (HTTP {0}: {1}).",
                    response.StatusCode,
                    response.StatusDescription));

                using (var streamReader = new StreamReader(response.GetResponseStream()))
                {
                  
                    DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Response));
                    object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());
                    _result = objResponse as Response;
                    var tipoCambioEuro = Convert.ToDecimal(_result.seriesResponse.series[0].Data[0].Data);
                    input.TipoCambioEuros = tipoCambioEuro;
                }


                string url2 = $"https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF60653/datos/2021-12-22/2021-12-22";
                HttpWebRequest request2 = WebRequest.Create(url) as HttpWebRequest;
                request2.Accept = "application/json";
                request2.Headers["Bmx-Token"] = "32e42f119b1b41c6eed92bf39c4cc5e5bd6706778c2c036ceb087cceb01bc5a0";
                HttpWebResponse response2 = request2.GetResponse() as HttpWebResponse;
                if (response2.StatusCode != HttpStatusCode.OK)
                    throw new Exception(String.Format(
                    "Server error (HTTP {0}: {1}).",
                    response2.StatusCode,
                    response2.StatusDescription));

                using (var streamReader = new StreamReader(response2.GetResponseStream()))
                {

                    DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Response));
                    object objResponse = jsonSerializer.ReadObject(response2.GetResponseStream());
                    _result2 = objResponse as Response;
                    var tipoCambioDolar = Convert.ToDecimal(_result2.seriesResponse.series[0].Data[0].Data);
                    input.TipoCambioDolar = tipoCambioDolar;
                }



                DynamicParameters dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@claveProducto", input.clave);
                dynamicParameters.Add("@tipoCambioDolar", input.TipoCambioDolar);
                dynamicParameters.Add("@tipoCambioEuro", input.TipoCambioEuros);
                var result = dataService.Get<ProductoDTO>("sp_get_Producto", dynamicParameters);

                if (result == null)
                    return NotFound("Registro no encontrado");
                else
                    return Ok(result);
                //
            }
            catch (Exception ex)
            {
                return BadRequest("La solicitud no se puede procesar en este momento, intente mas tarde");
            }

        }

        [HttpGet("GetProductosLeft")]
        public IActionResult GetProductosLeft()
        {
            try
            {
                var result = dataService.GetAll<ProductoDTO>("sp_get_ProductosSinPrecioLeft", null);
                if (result == null)
                    return BadRequest("La solicitud no se puede procesar en este momento, intente mas tarde");
                else
                    return Ok(result);

            }
            catch (Exception)
            {
                return BadRequest("La solicitud no se puede procesar en este momento, intente mas tarde");
            }

        }

        [HttpGet("GetProductosRight")]
        public IActionResult GetProductosRight()
        {
            try
            {
                var result = dataService.GetAll<ProductoDTO>("sp_get_ProductosSinPrecioRight", null);
                if (result == null)
                    return BadRequest("La solicitud no se puede procesar en este momento, intente mas tarde");
                else
                    return Ok(result);

            }
            catch (Exception)
            {
                return BadRequest("La solicitud no se puede procesar en este momento, intente mas tarde");
            }

        }
    }
}
