﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WebAppHerdez.DTOs;
using WebAppHerdez.DTOs.Productos;
using System.Net.Security;
using System.Xml.Serialization;
using System.Xml;
namespace WebAppHerdezApi.Connection
{
    public static class ConnectionService
    {
        public static ModelOutputGeneric GetConnectionWeb()
        {
            ModelOutputGeneric Response = new ModelOutputGeneric();
            try
            {

                string url = $"https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF46410/datos/2021-12-22/2021-12-22";
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.Accept = "application/json";
                request.Headers["Bmx-Token"] = "32e42f119b1b41c6eed92bf39c4cc5e5bd6706778c2c036ceb087cceb01bc5a0";
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception(String.Format(
                    "Server error (HTTP {0}: {1}).",
                    response.StatusCode,
                    response.StatusDescription));
             


                //var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://13.82.31.248:44300/sap/opu/odata/sap/API_SALES_QUOTATION_SRV/A_SalesQuotationItem(SalesQuotation='20000402',SalesQuotationItem='10')");
                //httpWebRequest.Credentials = new NetworkCredential("bpinst", "Welcome1");
                //httpWebRequest.ContentType = "application/xml";
                //httpWebRequest.Method = "GET";
                //ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });

                //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                //{
                //    var result = streamReader.ReadToEnd();
                //    string xmlString = result.ToString();

                //    XmlSerializer serializer = new XmlSerializer(typeof(ModelResponseApiSalesQuotationSRV));
                //    StringReader stringReader = new StringReader(xmlString);
                //    ModelResponseApiSalesQuotationSRV deserialized = (ModelResponseApiSalesQuotationSRV)serializer.Deserialize(stringReader);

                //    Response.ObjectGeneric = deserialized;
                //}

                Response.Success = true;
                Response.Message = "The process was successful";
            }
            catch (Exception ex)
            {
                Response.Message = "The operation cannot be completed at this time, please try again later";
                Response.Exception = (ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
            return Response;
        }
    }
}
