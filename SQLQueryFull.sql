
-- =============================================
-- Author: Ra�l Daniel Pe�a
-- Create date: 22 de Diciembre 2021
-- Description:	Se crea Base de Datos
-- =============================================
IF NOT EXISTS (SELECT * FROM sys.databases WHERE name='dbTestHerdez20211222')
	BEGIN
			CREATE DATABASE [dbTestHerdez20211222];	
	END
GO
	USE [dbTestHerdez20211222];		
GO
-- =============================================
-- Author: Ra�l Daniel Pe�a
-- Create date: 22 de Diciembre 2021
-- Description:	Se crea la tabla Productos
-- =============================================
CREATE TABLE Productos(
	[IdProducto]	INT PRIMARY KEY IDENTITY(1,1),
	[Clave]			VARCHAR(500) UNIQUE NOT NULL,
	[Nombre]		VARCHAR(500) UNIQUE NOT NULL,
	[IsActivo]		BIT NOT NULL,
	[FechaCreacion] DATETIME NOT NULL,
	[FechaUltimaModificacion] DATETIME NULL
);
GO
-- =============================================
-- Author: Ra�l Daniel Pe�a
-- Create date: 22 de Diciembre 2021
-- Description:	Se crea la tabla Precios
-- =============================================
CREATE TABLE Precios(
	[IdPrecio]	INT PRIMARY KEY IDENTITY(1,1),
	[IdProducto] INT NOT NULL,
	[Distancia]	Numeric(10,2) NULL,
	[PrecioPesos]	DECIMAL(10,2) NULL,
	[PrecioDolar]	DECIMAL(10,2) NULL,
	[PrecioEuros]	DECIMAL(10,2) NULL,
	[TipoCambioDolar]	DECIMAL(10,2) NULL,
	[TipoCambioEuros]	DECIMAL(10,2) NULL,
	[TotalEnvioPesos]	DECIMAL(10,2) NULL,
	[TotalEnvioDolar]	DECIMAL(10,2) NULL,
	[TotalEnvioEuros]	DECIMAL(10,2) NULL,
	[FechaCreacion] DATETIME NOT NULL,
	[FechaUltimaModificacion] DATETIME NULL

	CONSTRAINT FK_Precios_Productos FOREIGN KEY (IdProducto) REFERENCES [Productos](IdProducto)
);

GO
-- =============================================
-- Author: Ra�l Daniel Pe�a
-- Create date: 22 de Diciembre 2021
-- Description:	Se insertan registros a la tabla Productos
-- =============================================
INSERT INTO [dbo].[Productos]
           ([Clave]
           ,[Nombre]
           ,[IsActivo]
           ,[FechaCreacion])
     VALUES
           ('SA001'--<Clave, varchar(500),>
           ,'Pure de tomate'--<Nombre, varchar(500),>
           ,1--<IsActivo, bit,>
           ,GETDATE()--<FechaCreacion, datetime,>
           ),
		   ('JU001'--<Clave, varchar(500),>
           ,'Jugo de naranja'--<Nombre, varchar(500),>
           ,1--<IsActivo, bit,>
           ,GETDATE()--<FechaCreacion, datetime,>
           ),
		   ('VE001'--<Clave, varchar(500),>
           ,'Ensalada de Verduras '--<Nombre, varchar(500),>
           ,1--<IsActivo, bit,>
           ,GETDATE()--<FechaCreacion, datetime,>
           ),
		   ('SA002'--<Clave, varchar(500),>
           ,'Salsa Bufalo'--<Nombre, varchar(500),>
           ,1--<IsActivo, bit,>
           ,GETDATE()--<FechaCreacion, datetime,>
           ),
		   ('JU002'--<Clave, varchar(500),>
           ,'Jugo de Manzana'--<Nombre, varchar(500),>
           ,1--<IsActivo, bit,>
           ,GETDATE()--<FechaCreacion, datetime,>
           )
GO
-- =============================================
-- Author: Ra�l Daniel Pe�a
-- Create date: 22 de Diciembre 2021
-- Description:	Se insertan registros a la tabla Precios
-- =============================================
INSERT INTO [dbo].[Precios]
           ([IdProducto] 
           ,[Distancia]
		   ,[PrecioPesos]
		   ,[PrecioDolar]
		   ,[PrecioEuros]
           ,[TipoCambioDolar]
           ,[TipoCambioEuros]
           ,[TotalEnvioPesos]
           ,[TotalEnvioDolar]
           ,[TotalEnvioEuros]
           ,[FechaCreacion]
           )
     VALUES
           (
           1--<IdProducto, int,>
		   ,1000--<Distancia, numeric(10,2),>
		   ,150.50--<PrecioPesos, decimal(10,2),>
		   ,0--<PrecioDolar, decimal(10,2),>
		   ,0--<PrecioEuros, decimal(10,2),>
           ,0--<TipoCambioDolar, decimal(10,2),>
           ,0--<TipoCambioEuros, decimal(10,2),>
           ,50.50--<TotalEnvioPesos, decimal(10,2),>
           ,0--<TotalEnvioDolar, decimal(10,2),>
           ,0--<TotalEnvioEuros, decimal(10,2),>
           ,GETDATE()--<FechaCreacion, datetime,>
           ),
		   (
		   2--<IdProducto, int,>
		   ,2000--<Distancia, numeric(10,2),>
		   ,250.50--<PrecioPesos, decimal(10,2),>
		   ,0--<PrecioDolar, decimal(10,2),>
		   ,0--<PrecioEuros, decimal(10,2),>
           ,0--<TipoCambioDolar, decimal(10,2),>
           ,0--<TipoCambioEuros, decimal(10,2),>
           ,20.50--<TotalEnvioPesos, decimal(10,2),>
           ,0--<TotalEnvioDolar, decimal(10,2),>
           ,0--<TotalEnvioEuros, decimal(10,2),>
           ,GETDATE()--<FechaCreacion, datetime,>
           ),
		   (
		   3--<IdProducto, int,>
		   ,3000--<Distancia, numeric(10,2),>
		   ,350.50--<PrecioPesos, decimal(10,2),>
		   ,0--<PrecioDolar, decimal(10,2),>
		   ,0--<PrecioEuros, decimal(10,2),>
           ,0--<TipoCambioDolar, decimal(10,2),>
           ,0--<TipoCambioEuros, decimal(10,2),>
           ,30.50--<TotalEnvioPesos, decimal(10,2),>
           ,0--<TotalEnvioDolar, decimal(10,2),>
           ,0--<TotalEnvioEuros, decimal(10,2),>
           ,GETDATE()--<FechaCreacion, datetime,>
           ),
		    (
		   4--<IdProducto, int,>
		   ,4000--<Distancia, numeric(10,2),>
		   ,450.50--<PrecioPesos, decimal(10,2),>
		   ,0--<PrecioDolar, decimal(10,2),>
		   ,0--<PrecioEuros, decimal(10,2),>
           ,0--<TipoCambioDolar, decimal(10,2),>
           ,0--<TipoCambioEuros, decimal(10,2),>
           ,40.50--<TotalEnvioPesos, decimal(10,2),>
           ,0--<TotalEnvioDolar, decimal(10,2),>
           ,0--<TotalEnvioEuros, decimal(10,2),>
           ,GETDATE()--<FechaCreacion, datetime,>
           ),
		   (
		   5--<IdProducto, int,>
		   ,5000--<Distancia, numeric(10,2),>
		   ,550.50--<PrecioPesos, decimal(10,2),>
		   ,0--<PrecioDolar, decimal(10,2),>
		   ,0--<PrecioEuros, decimal(10,2),>
           ,0--<TipoCambioDolar, decimal(10,2),>
           ,0--<TipoCambioEuros, decimal(10,2),>
           ,50.50--<TotalEnvioPesos, decimal(10,2),>
           ,0--<TotalEnvioDolar, decimal(10,2),>
           ,0--<TotalEnvioEuros, decimal(10,2),>
           ,GETDATE()--<FechaCreacion, datetime,>
           )
GO

-- =============================================
-- Author: Ra�l Daniel Pe�a
-- Create date: 22 de Diciembre 2021
-- Description:	Obtiene Productos Sin Precio por lefth
-- =============================================
CREATE PROCEDURE sp_get_ProductosSinPrecioLeft
	-- Add the parameters for the stored procedure here	
AS
BEGIN TRY
		
	SELECT	Pro.[Clave]
			,Pro.[Nombre]
			,Pro.[IsActivo]
			,Pro.[FechaCreacion]
			,Pro.[FechaUltimaModificacion]			
	FROM	[dbTestHerdez20211222].[dbo].[Productos] AS Pro
			LEFT JOIN [dbTestHerdez20211222].[dbo].[Precios] AS Pre ON Pro.IdProducto=Pre.IdProducto
			WHERE Pre.PrecioPesos IS NULL;
		
END TRY
BEGIN CATCH
	SELECT 0 as Success , ERROR_MESSAGE() AS Result;
END CATCH
GO

-- =============================================
-- Author: Ra�l Daniel Pe�a
-- Create date: 22 de Diciembre 2021
-- Description:	Obtiene Productos Sin Precio por Right
-- =============================================
CREATE PROCEDURE sp_get_ProductosSinPrecioRight
	-- Add the parameters for the stored procedure here	
AS
BEGIN TRY
		
	SELECT	Pro.[Clave]
			,Pro.[Nombre]
			,Pro.[IsActivo]
			,Pro.[FechaCreacion]
			,Pro.[FechaUltimaModificacion]
	FROM	[dbTestHerdez20211222].[dbo].[Precios] AS Pre 
			RIGHT JOIN  [dbTestHerdez20211222].[dbo].[Productos] AS Pro ON Pro.IdProducto=Pre.IdProducto
			WHERE Pre.PrecioPesos IS NULL;	
END TRY
BEGIN CATCH
	SELECT 0 as Success , ERROR_MESSAGE() AS Result;
END CATCH
GO

-- =============================================
-- Author: Ra�l Daniel Pe�a
-- Create date: 22 de Diciembre 2021
-- Description:	Obtiene Producto
-- =============================================
CREATE PROCEDURE sp_get_Producto
	-- Add the parameters for the stored procedure here
	@claveProducto   AS VARCHAR (500),
	@tipoCambioDolar AS DECIMAL(10,2),
	@tipoCambioEuro  AS DECIMAL(10,2)
AS
BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @idProducto AS INT;
			DECLARE @precioPesos AS DECIMAL(10,2); 
			DECLARE @totalEnvioPesos AS DECIMAL(10,2);
			

			IF EXISTS (SELECT TOP 1 IdProducto FROM Productos WHERE Clave=@claveProducto) BEGIN
				SET @idProducto=(SELECT TOP 1 IdProducto FROM Productos WHERE Clave=@claveProducto);
				SET @precioPesos=(SELECT TOP 1 PrecioPesos FROM Precios WHERE IdProducto=@idProducto);
				SET @totalEnvioPesos=(SELECT TOP 1 PrecioPesos FROM Precios WHERE IdProducto=@idProducto);
				
				UPDATE [dbo].[Precios]
					   SET [PrecioDolar] = (@precioPesos * @tipoCambioDolar)
					      ,[PrecioEuros] = (@precioPesos * @tipoCambioEuro)	
					      ,[TipoCambioDolar] = @tipoCambioDolar
					      ,[TipoCambioEuros] = @tipoCambioEuro
					      ,[TotalEnvioDolar] = ((@totalEnvioPesos * @tipoCambioDolar ) * 13.28)
					      ,[TotalEnvioEuros] = ((@totalEnvioPesos * @tipoCambioEuro ) * 19.87)
					      ,[FechaUltimaModificacion] =GETDATE()
					 WHERE IdProducto=@idProducto;
				
				SELECT	TOP 1 PRO.[IdProducto]
						,PRO.[Clave]
						,PRO.[Nombre]
						,PRO.[IsActivo]
						,PRO.[FechaCreacion] AS FechaCreacionProducto
						,PRO.[FechaUltimaModificacion] AS FechaUltimaModificacionProducto
						,PRE.[Distancia]
						,PRE.[PrecioPesos]
						,PRE.[PrecioDolar]
						,PRE.[PrecioEuros]
						,PRE.[TipoCambioDolar]
						,PRE.[TipoCambioEuros]
						,PRE.[TotalEnvioPesos]
						,PRE.[TotalEnvioDolar]
						,PRE.[TotalEnvioEuros]
						,PRE.[FechaCreacion] AS FechaCreacionProducto
						,PRE.[FechaUltimaModificacion] AS FechaUltimaModificacionProducto
				FROM	[dbTestHerdez20211222].[dbo].[Productos] AS PRO
						INNER JOIN [dbTestHerdez20211222].[dbo].[Precios] AS PRE ON Pro.IdProducto=Pre.IdProducto;
			END

			ELSE BEGIN
				SELECT 0 as Success , 'El producto no existe!!' AS Result;
			END		
		COMMIT;
END TRY
BEGIN CATCH
	ROLLBACK;
	SELECT 0 as Success ,CONCAT ( 'No se puedo obtener el producto - ',ERROR_MESSAGE()) AS Result;
END CATCH
GO