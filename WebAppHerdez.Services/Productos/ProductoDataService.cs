﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAppHerdez.Services.Productos
{
    public class ProductoDataService:IProductoDataService
    {
        private readonly string connectioString;
        public ProductoDataService(string _connectionstring)
        {
            connectioString = _connectionstring;
        }
        public T Get<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure)
        {
            using (IDbConnection db = dbConnection)
            {
                try
                {
                    db.Open();
                    return db.QueryFirstOrDefault<T>(sp, parms, commandType: commandType);
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }

        public List<T> GetAll<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.StoredProcedure)
        {
            using (IDbConnection db = dbConnection)
            {
                try
                {
                    db.Open();
                    return db.Query<T>(sp, parms, commandType: commandType).ToList();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public IDbConnection dbConnection
        {
            get
            {
                return new SqlConnection(connectioString);
            }
        }     
    }
}
