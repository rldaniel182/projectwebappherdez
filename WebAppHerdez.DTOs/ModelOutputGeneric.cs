﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppHerdez.DTOs.Productos
{
    public class ModelOutputGeneric
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public dynamic ObjectGeneric { get; set; }
    }
}