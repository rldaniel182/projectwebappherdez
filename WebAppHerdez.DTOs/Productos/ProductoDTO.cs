﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAppHerdez.DTOs.Productos
{
    public class ProductoDTO
    {
        // Detalles Producto
        public int idProducto { get; set; }
        public string clave { get; set; }
        public string nombre { get; set; }
        public Boolean isActivo { get; set; }
        public DateTime fechaCreacionProducto { get; set; }
        public DateTime fechaUltimaModificacionProducto { get; set; }

        // Precio Producto
        public int IdPrecio { get; set; }
        public decimal Distancia { get; set; }
        public decimal PrecioPesos { get; set; }
        public decimal PrecioEuros { get; set; }
        public decimal TipoCambioDolar { get; set; }
        public decimal TipoCambioEuros { get; set; }
        public decimal TotalEnvioPesos { get; set; }
        public decimal TotalEnvioDolar { get; set; }
        public decimal TotalEnvioEuros { get; set; }
        public DateTime FechaCreacionPrecio { get; set; }
        public DateTime FechaUltimaModificacionPrecio { get; set; }
    }
}
