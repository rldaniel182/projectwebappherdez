﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAppHerdez.DTOs.Productos
{
    public class ResponseDTO
    {
        public int IdResponse { get; set; }
        public string Body { get; set; }
        public dynamic ObjectGeneric { get; set; }
    }
}
